use std::cell::RefCell;
use derailleur_app as app;

mod state;

fn main() {
    let window = RefCell::new(
		app::OpenGLWindow::new("derailleur-sample".to_string(), 360, 640)
    );
    
    let mut app = app::build_singleton_app(
        window,
        state::MessageMain::new() //initial state
    );
    app.run();
}
