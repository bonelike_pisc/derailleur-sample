use std::rc::Rc;
use std::cell::RefCell;
use piston::input;
use derailleur_app::UIInput;
use derailleur_app::state::{State, NextAction, StateRuntimeError};
use derailleur_draw::Draw;
use derailleur_draw_ext::dynamic::StretchingText;
use derailleur_resource::ResourceLoaderFactory;

pub struct MessageSub {
    messages: Vec<String>,
    text: Option<Rc<RefCell<dyn Draw>>>, //cache for the purpose of keeping it on memory
    go_next: bool //flag whether go to the next state
}

impl MessageSub {
    pub fn new(messages: Vec<String>) -> Self {
        Self {
            messages: messages,
            text: None,
            go_next: false
        }
    }
}

impl State for MessageSub {
    fn initialize(&mut self) -> Result<(), StateRuntimeError> {
        let font = ResourceLoaderFactory::ttc().load("resource/mplus-1c-regular.ttf").unwrap();
        let message = match self.messages.len() {
            0 => "[ERROR: no messages to display]",
            _ => self.messages.get(0).unwrap(),
        };
        let _text = StretchingText::new(
            message,
            100, //stretching cycle (ms)
            Rc::new(font),
            20, //font size
            [40.0, 280.0], //position
            [1.0, 1.0, 1.0, 1.0] //font color
        );
        let text = Rc::new(RefCell::new(derailleur_draw::dynamic::make_draw(_text)));
        derailleur_global::main_canvas().append_drawee_to_front(text.clone());
        self.text = Some(text);
        Ok(())
    }

    fn on_input(&mut self, input: UIInput) -> Result<(), StateRuntimeError> {
        if input.is_key_pressed(input::keyboard::Key::Space) {
            self.go_next = true
        }
        Ok(())
    }

    fn next_action(&self) -> NextAction {
        if self.go_next {
            if self.messages.len() <= 1 {
                NextAction::Terminate
            }
            else {
                NextAction::ChangeState
            }
        }
        else {
            NextAction::Continue
        }
    }
    
    fn next_state(&self) -> Result<Box<dyn State>, StateRuntimeError> {
        if self.messages.len() <= 1 {
            Err(StateRuntimeError::from_message("unexpected call"))
        }
        else {
            // the 2nd & later messages are passed to the next state.
            let next_messages: Vec<String> = self.messages.iter()
                .enumerate()
                .filter(|&(i, _s)| i > 0)
                .map(|(_i, s)| s)                
                .cloned().collect();
            let next_state = Box::new(MessageSub::new(next_messages));
            Ok(next_state)
        }
    }
}