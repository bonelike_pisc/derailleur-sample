use std::rc::Rc;
use std::cell::RefCell;
use derailleur_app::UIInput;
use derailleur_app::state::{State, StateChain, NextAction, StateRuntimeError};
use derailleur_draw::Sprite;
use derailleur_resource::ResourceLoaderFactory;
use derailleur_geometry2d::Rectangle;
use super::MessageSub;

pub struct MessageMain {
    sub_state: Option<StateChain>,
    window: Option<Rc<RefCell<Sprite>>> //cache for the purpose of keeping it on memory
}

impl MessageMain {
    pub fn new() -> Self {
        Self {
            sub_state: None,
            window: None,
        }
    }
}

impl State for MessageMain {
    fn initialize(&mut self) -> Result<(), StateRuntimeError> {
        // create message window
        let texture = ResourceLoaderFactory::texture().load("resource/message-window.png").unwrap();
        let _window = Rc::new(RefCell::new(Sprite::from_gl_texture(
            Rc::new(texture), 
            Rectangle::from_array([20.0, 250.0, 600.0, 100.0])
        )));
        derailleur_global::main_canvas().append_drawee(_window.clone(), 0);
        self.window = Some(_window);

        // create sub-state
        let json = ResourceLoaderFactory::text().load("resource/messages.json").unwrap();
        let messages: Vec<String> = serde_json::from_str(&json).unwrap();
        let _sub = MessageSub::new(messages);
        self.sub_state = Some(StateChain::new(_sub));
        Ok(())
    }

    fn update(&mut self, dt_sec: f64) -> Result<(), StateRuntimeError> {
        self.sub_state.as_mut().unwrap().update(dt_sec)
    }

    fn on_input(&mut self, input: UIInput) -> Result<(), StateRuntimeError> {
        self.sub_state.as_mut().unwrap().on_input(input)
    }

    fn next_action(&self) -> NextAction {
        self.sub_state.as_ref().unwrap().next_action()
    }
}


