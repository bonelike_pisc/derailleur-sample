derailleurを使用したサンプルコードです。
メッセージウインドウを表示します。
スペースキーで次のメッセージを表示、最後のメッセージでスペースキーを押下すると終了します。

### 使用素材

* フォント
    * M+ Fonts - mplus-1c-regular.ttf

          [https://mplus-fonts.osdn.jp](https://mplus-fonts.osdn.jp/)

### 実行方法

1. Rustのインストール

    プログラムのビルドにはRustが必要です。インストール済みでない場合は、下記のページを参考にRustをインストールします。
    
    https://doc.rust-jp.rs/book/second-edition/ch01-01-installation.html

2. ソースコードのダウンロード

    次の2つのリポジトリをダウンロードします。

    * derailleur-sample
    
          (https://bitbucket.org/bonelike_pisc/derailleur-sample)
          ※このリポジトリ

    * derailleur
    
          (https://bitbucket.org/bonelike_pisc/derailleur)

    ※ページ内の「ダウンロード」をクリック

3. ソースコードの展開

    2.でダウンロードしたzipファイルをそれぞれ展開します。
    展開してできるディレクトリ（フォルダ）が同じディレクトリの下に並ぶように配置してください。
    
    例）
    
        parent-dir 
        　|
        　|- derailleur-sample
        　|
        　|- derailleur

4. ビルド

    コマンドライン上でderailleur-sampleに移動し、次のコマンドを実行します。
    
    ```
    cargo build
    ```
    
    場合によっては数分間かかります。画面上に"Finished"と表示されれば完了です。

5. 実行

    コマンドライン上でderailleur-sampleに移動し、次のコマンドを実行します。
    
    ```
    cargo run
    ```
    
    ウインドウが現れ、メッセージウインドウが表示されれば成功です。
